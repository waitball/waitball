# WaitBall

WaitBall is a crowdsourced app for finding the closest alternative restaurant when the wait at your favorite restaurant is just too long

# [Demo Video][1]

Learn more about WaitBall and see a demo of the mobile app here: [https://www.youtube.com/watch?v=hs0q10SuMVI][1]

# [Source Code][2]

Browse the original source code for the WaitBall backend and WaitBall mobile app: [https://bitbucket.org/waitball/waitball][2]



# Application Links

## [Android Application APK][3]

Download a copy of the WaitBall Android app here: [http://waitball-app.herokuapp.com/assets/waitball-android.apk][3]

![Mobile App Screenshot](http://waitball-app.herokuapp.com/assets/mobile-screenshot-5.png)




[1]: https://www.youtube.com/watch?v=hs0q10SuMVI
[2]: https://bitbucket.org/waitball/waitball
[3]: http://waitball-app.herokuapp.com/assets/waitball-android.apk