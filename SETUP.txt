
Overview:
	This is a skeleton for a generic responsive web + mobile app featuring:
	* AngularJS / Material Design web app
	* Cordova / Ionic mobile app
	* Node / Express web server + proxy to API server
	* Python / Flask API server
	* Mongo / Database

Setup:
	# export WWW_SERVER_URL=http://ip.add.re.ss:5000
	# export API_URL=http://ip.add.re.ss:3000
	# export MOBILE_SERVER_URL=http://ip.add.re.ss:5000
	# export MONGOLAB_URI=mongodb://127.0.0.1/skeletor
	# pip install -r requirements.txt
	# npm install
	# cd mobileapp
		# cordova prepare
		# ionic resources --icon




Running Locally:
	API Server (port 3000):
		# python -m api.server
	Web Server (port 5000):
		# node web.js
	Mobile App Debug Server (port 4000):
		# node mobile.js


Running Tests:
	# nosetests --nocapture api.tests
    
Running Mobile App on Android Phone:
	Web Server (port 5000):
		# node web.js
	Build and Install App:
		# cordova run android

Adding to Heroku:
	export APP_NAME=waitball

	heroku apps:create $APP_NAME-api --remote api
	heroku buildpacks:set --app $APP_NAME-api https://github.com/heroku/heroku-buildpack-python
	heroku config:set --app $APP_NAME-api API_URL=http://$APP_NAME-api.herokuapp.com GOOGLE_API_KEY=$GOOGLE_API_KEY MOBILE_SERVER_URL=http://$APP_NAME-app.herokuapp.com SUBPROC_TYPE=python WWW_SERVER_URL=http://$APP_NAME-app.herokuapp.com
	heroku config:set --app $APP_NAME-api YELP_CONSUMER_KEY=$YELP_CONSUMER_KEY YELP_CONSUMER_SECRET=$YELP_CONSUMER_SECRET YELP_TOKEN=$YELP_TOKEN YELP_TOKEN_SECRET=$YELP_TOKEN_SECRET
	heroku addons:create --app $APP_NAME-api mongolab

	heroku apps:create $APP_NAME-app --remote app
	heroku buildpacks:set --app $APP_NAME-app https://github.com/heroku/heroku-buildpack-nodejs
	heroku config:set --app $APP_NAME-app API_URL=http://$APP_NAME-api.herokuapp.com GOOGLE_API_KEY=$GOOGLE_API_KEY MOBILE_SERVER_URL=http://$APP_NAME-app.herokuapp.com SUBPROC_TYPE=node WWW_SERVER_URL=http://$APP_NAME-app.herokuapp.com

