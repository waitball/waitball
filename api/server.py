import os
import sys
sys.path.append(os.path.dirname(__file__))

import json
import dateutil.parser
import re
import random
import requests
import pymongo
from time import sleep
from pymongo import MongoClient
from bson.objectid import ObjectId
from urlparse import urlparse
from copy import deepcopy
from datetime import datetime, timedelta
import pytz
import urllib
from geopy.distance import great_circle
from geopy.geocoders import Nominatim

import yelp.client
from yelp.oauth1_authenticator import Oauth1Authenticator
import yelp.obj.response_object
def make_yelp_proxy_init(old_init):
    def yelp_proxy_init(self, response):
        self._response = response
        old_init(self, response)
    return yelp_proxy_init
yelp.obj.response_object.ResponseObject.__init__ = make_yelp_proxy_init(yelp.obj.response_object.ResponseObject.__init__)


from flask import Flask, request, send_from_directory, safe_join, Response
from flask.ext.cors import CORS
from collections import Counter
app = Flask(__name__)
CORS(app)

MONGO_URL = os.environ['MONGOLAB_URI']
MONGO_CLIENT = MongoClient(MONGO_URL)
MONGO_DB = MONGO_CLIENT[urlparse(MONGO_URL).path[1:]]

GOOGLE_API_KEY = os.environ['GOOGLE_API_KEY']

WWW_SERVER_URL = os.environ['WWW_SERVER_URL']

GEOLOCATOR = Nominatim()

YELP_AUTH = Oauth1Authenticator(
    consumer_key=os.environ['YELP_CONSUMER_KEY'],
    consumer_secret=os.environ['YELP_CONSUMER_SECRET'],
    token=os.environ['YELP_TOKEN'],
    token_secret=os.environ['YELP_TOKEN_SECRET']
)
YELP_CLIENT = yelp.client.Client(YELP_AUTH)


def dump(filename, content):
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    with open(filename, 'w') as w:
        w.write(content)

def load(filename):
    with open(filename, 'r') as r:
        return r.read()
        
def jdump(jval, filename):
    jdir = os.path.dirname(filename)
    if jdir and not os.path.exists(jdir):
        os.makedirs(jdir)
    with open(filename, 'w') as w:
        json.dump(jval, w, indent=2)

def jload(filename):
    with open(filename, 'r') as r:
        return json.load(r)        

def dist(a,b):
    return great_circle(a,b).meters


@app.route("/api/test")
def sample():
    resp = {"Testing": "Hello world!"}
    return Response(json.dumps(resp), mimetype='application/json')

@app.route("/api/test/echo/<arg>", methods=['GET', 'PUT', 'DELETE', 'POST'])
def sample_echo(arg):
    if request.method in ['POST','PUT']:
        req = json.loads(request.get_data())
    else:
        req = {}
    resp = {"Testing Arg": arg,
            "Testing Method": request.method,
            "Testing Data": req}
    return Response(json.dumps(resp), mimetype='application/json')

def utcnow():
    return datetime.utcnow().replace(tzinfo=pytz.UTC)



@app.route("/api/users/<user_id>", methods=['GET','POST'])
def user_info(user_id):
    user_profile = MONGO_DB.users.find_one({'user_id': user_id}) or {'user_id': user_id,
                     'name': 'Anonymous',
                     'photo_url': WWW_SERVER_URL+'/profiles/anonymous.jpg'
                    }
    
    if '_id' in user_profile:
        del user_profile['_id']
    
    if request.method in ['POST']:
        req = json.loads(request.get_data())
        req['user_id'] = user_id
        for k in user_profile:
            if req.get(k):
                user_profile[k] = req[k]
        MONGO_DB.users.update_one( {'user_id': user_id}, {'$set': user_profile }, upsert=True )

    return Response(json.dumps(user_profile), mimetype='application/json')

def get_wait_times(business_ids):
    nowstr = utcnow().isoformat()
    waits = dict([(b,{'business_id': b, 
                      'wait_minutes': 0.0,
                      'timestamp': '2000-01-01T01:00:00.000000+00:00'}) for b in business_ids])
    for wait in MONGO_DB.waits.find({'business_id': {'$in': business_ids}}):
        del wait['_id']
        if wait['timestamp'] > waits[wait['business_id']]['timestamp']:
            waits[wait['business_id']] = wait
    return [waits[b] for b in business_ids]

def business_cache_save(businesses):
    if not businesses:
        return
    cached_items = [cb['id'] for cb in MONGO_DB.businesses.find({'id': {'$in': [b['id'] for b in businesses]}})]
    save_items = [b for b in businesses if b['id'] not in cached_items]
    if save_items:
        bulk = MONGO_DB.businesses.initialize_unordered_bulk_op()
        for business in save_items:
            bulk.find({'id': business['id']}).upsert().replace_one(business)
        bulk.execute()

def business_cache_get(business_ids):
    if not business_ids:
        return []
    cached_items = dict([(cb['id'],cb) for cb in MONGO_DB.businesses.find({'id': {'$in': business_ids}})])
    for v in cached_items.values():
        del v['_id']
    save_ids = [b for b in business_ids if b not in cached_items]
    for business_id in save_ids:
        business = YELP_CLIENT.get_business(business_id)._response
        cached_items[business['id']] = business
    if [s for s in save_ids if s in cached_items]:
        bulk = MONGO_DB.businesses.initialize_unordered_bulk_op()
        for business_id, business in [(k,v) for k,v in cached_items.iteritems() if k in save_ids]:
            bulk.find({'id': business['id']}).upsert().replace_one(business)
        bulk.execute()

    waits = dict([(w['business_id'],w) for w in get_wait_times(business_ids)])

    businesses = [cached_items[b] for b in business_ids if b in cached_items]
    for business in businesses:
        wait = waits[business['id']]
        if wait['wait_minutes'] == 0:
            wait_minutes = random.choice([0,0,0,15,15,15,30,30,45,45,60,90])
            business_id = business['id']
            user_id = 'demo'+str(random.randint(1,100))
            wait = report_wait(user_id, business_id, wait_minutes)
        business['wait'] = wait
    return businesses
            
    
@app.route("/api/nearby", methods=['POST'])
def items_nearby():
    if request.method in ['POST']:
        req = json.loads(request.get_data())
        coords = [req['lat'], req['lon']]
        radius = 50
        search_result = YELP_CLIENT.search_by_coordinates(coords[0], coords[1], sort=1, radius_filter=radius, category_filter='restaurants')
        business_cache_save([b._response for b in search_result.businesses if b._response.get('coordinate')])
        resp = [b._response for b in search_result.businesses if b.distance < radius]
        resp.sort(key=lambda x: x['distance'])
        return Response(json.dumps(resp), mimetype='application/json')
 
@app.route("/api/wait", methods=['POST'])
def bulk_wait_times():
    if request.method in ['POST']:
        req = json.loads(request.get_data())
        resp = get_wait_times(req)
        return Response(json.dumps(resp), mimetype='application/json')


def report_wait(user_id, business_id, wait_minutes):
    wait = {'business_id': business_id,
            'user_id': user_id,
            'wait_minutes': float(wait_minutes),
            'timestamp': utcnow().isoformat(),
            }
    MONGO_DB.waits.insert_one(wait)
    if '_id' in wait:
        del wait['_id']
    return wait


@app.route("/api/users/<user_id>/wait/<business_id>", methods=['POST', 'GET'])
def wait_times(user_id, business_id):
    if request.method in ['POST']:
        req = json.loads(request.get_data())
        report_wait(user_id, business_id, float(req['wait_minutes']))
    resp = get_wait_times([business_id])[0]
    return Response(json.dumps(resp), mimetype='application/json')

@app.route("/api/feed", methods=['GET'])
def activity_feed():
    items = [w for w in MONGO_DB.waits.find({'timestamp': {'$lt': utcnow().isoformat()}}).sort("timestamp",pymongo.DESCENDING).limit(10)]
    users = dict([(u['user_id'], u) for u in MONGO_DB.users.find({'user_id': {'$in': [item.get('user_id') for item in items if item.get('user_id')]}})])
    for u in users.values():
        del u['_id']
    businesses = dict([(b['id'], b) for b in MONGO_DB.businesses.find({'id': {'$in': [item.get('business_id') for item in items if item.get('business_id')]}})])
    for b in businesses.values():
        del b['_id']
    for item in items:
        del item['_id']
        if item.get('user_id') in users:
            item['user'] = users[item.get('user_id')]
        if item.get('business_id') in businesses:
            item['business'] = businesses[item.get('business_id')]
    resp = items
    return Response(json.dumps(resp), mimetype='application/json')

@app.route("/api/details/<business_id>", methods=['GET'])
def business_details(business_id):
    resp = business_cache_get([business_id])[0]
    return Response(json.dumps(resp), mimetype='application/json')

  
@app.route("/api/recommended/<business_id>", methods=['GET'])
def recommended(business_id):
    business = business_cache_get([business_id])[0]
    lat = business.get('location',{}).get('coordinate',{}).get('latitude')
    lon = business.get('location',{}).get('coordinate',{}).get('longitude')
    categories = [c for c in business.get('categories', [])]

    result_set = []
    if lat and lon:
        search_result = [b._response for b in YELP_CLIENT.search_by_coordinates(lat, lon, sort=1, category_filter='restaurants').businesses][:10]
        if search_result:
            business_cache_save(search_result)
            result = {'title': "Close to "+business.get('name','you'),
                      'businesses': business_cache_get([b['id'] for b in search_result])}
            result_set.append(result)
    for cat_name, cat_code in categories:
        search_result = [b._response for b in YELP_CLIENT.search_by_coordinates(lat, lon, sort=0, category_filter='restaurants,'+cat_code).businesses][:10]
        if search_result:
            business_cache_save(search_result)
            result = {'title': cat_name,
                      'businesses': business_cache_get([b['id'] for b in search_result])}
            result_set.append(result)

    resp = result_set    
    return Response(json.dumps(resp), mimetype='application/json')
    

@app.route("/api/recommended", methods=['POST'])
def recommended_default():
    if request.method in ['POST']:
        req = json.loads(request.get_data())
        lat = float(req['lat'])
        lon = float(req['lon'])
        
        result_set = []
        if lat and lon:
            search_result = [b._response for b in YELP_CLIENT.search_by_coordinates(lat, lon, sort=1, category_filter='restaurants').businesses][:10]
            if search_result:
                business_cache_save(search_result)
                result = {'title': "Close to you",
                          'businesses': business_cache_get([b['id'] for b in search_result])}
                result_set.append(result)

        resp = result_set    
        return Response(json.dumps(resp), mimetype='application/json')
    

@app.route("/api/map/<business_id>", methods=['GET'])
def business_map(business_id):
    map_filename = os.path.join(os.path.dirname(__file__), 'map_cache/business_'+business_id+'.png')
    business = business_cache_get([business_id])[0]
    coord_str = str(business['location']['coordinate']['latitude'])+","+str(business['location']['coordinate']['longitude'])

    if not os.path.exists(map_filename):            
        map_url = 'https://maps.googleapis.com/maps/api/staticmap'
        map_params = {'size': '335x130',
                      'markers': [ coord_str ],
                      'key': GOOGLE_API_KEY,
                      }
        resp = requests.get(map_url, map_params)
        if resp.status_code == 200:
            with open(map_filename, 'wb') as w:
                w.write(resp.content)
    with open(map_filename) as r:
        map_data = r.read()
        return Response(map_data, mimetype='image/png')


@app.route("/api/reset", methods=['GET'])
def reset():
    if request.args.get('key') != 'kwyjibo':
        return Response(json.dumps({'status': 'reset ignored'}), mimetype='application/json')

    MONGO_DB.users.drop()
    MONGO_DB.users.create_index('user.user_id')

    MONGO_DB.businesses.drop()
    MONGO_DB.businesses.create_index('id')

    MONGO_DB.waits.drop()
    MONGO_DB.waits.create_index('business_id')
    MONGO_DB.waits.create_index('user_id')
    MONGO_DB.waits.create_index('timestamp')
    

    men_names = [   'Michael',
                    'Christopher',
                    'Jason',
                    'David',
                    'James',
                    'Matthew',
                    'Joshua',
                    'John',
                    'Robert',
                    'Joseph',
                    'Daniel',
                    'Brian',
                    'Justin',
                    'William',
                    'Ryan',
                    'Eric',
                    'Nicholas',
                    'Jeremy',
                    'Andrew',
                    'Timothy',
                    ]
    women_names = [ 'Jennifer',
                    'Amanda',
                    'Jessica',
                    'Melissa',
                    'Sarah',
                    'Heather',
                    'Nicole',
                    'Amy',
                    'Elizabeth',
                    'Michelle',
                    'Kimberly',
                    'Angela',
                    'Stephanie',
                    'Tiffany',
                    'Christina',
                    'Lisa',
                    'Rebecca',
                    'Crystal',
                    'Kelly',
                    'Erin',
                    ]

    user_profiles = []
    for i in range(53):
        user_profiles.append({'user_id': 'demo'+str(len(user_profiles) + 1),
                              'username': random.choice(men_names),
                              'photo_url': WWW_SERVER_URL+'/profiles/man'+str(i%50)+'.jpg'
                              })
    for i in range(54):
        user_profiles.append({'user_id': 'demo'+str(len(user_profiles) + 1),
                              'username': random.choice(women_names),
                              'photo_url': WWW_SERVER_URL+'/profiles/woman'+str(i%50)+'.jpg'
                              })

    MONGO_DB.users.insert_many(user_profiles)
    for user in user_profiles:
        del user['_id']
    
    return Response(json.dumps({'status': 'reset_complete',
                                'timestamp': utcnow().isoformat(),
                                }), mimetype='application/json')
    
    




    
if __name__ == "__main__":
    app.run('0.0.0.0', 3000, debug=True)
    pass
