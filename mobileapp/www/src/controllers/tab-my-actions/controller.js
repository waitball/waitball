document.APP_MODULES = document.APP_MODULES || [];

(function(){

var CONTROLLER_URL = document.currentScript.src;
var TEMPLATE_URL = CONTROLLER_URL.replace('controller.js','view.html');
var CONTROLLER_PATH = URI(CONTROLLER_URL).path();
CONTROLLER_PATH = CONTROLLER_PATH.substring(CONTROLLER_PATH.indexOf('/src/controllers/'));

var ROUTE_URL = '/my-actions';
var MODULE_NAME = 'mainApp'+CONTROLLER_PATH.replace('/src','').replace('/controller.js','').replace(/\//g,'.');
var CONTROLLER_NAME = MODULE_NAME.replace(/\./g,'_').replace(/-/g,'_');
document.APP_MODULES.push(MODULE_NAME);

console.log(MODULE_NAME, "Registering route", ROUTE_URL);
angular.module(MODULE_NAME, ['ionic'])
  .config(function($stateProvider) {
    $stateProvider.state('tab.my-actions', {
        url: ROUTE_URL,
        views: {
          'tab-my-actions': {
            templateUrl: TEMPLATE_URL,
            controller: CONTROLLER_NAME
          }
        }
      });
  })
  .controller(CONTROLLER_NAME, function($scope, itemSearchService, userService, $state, CLIENT_SETTINGS) {
    console.log("Instantiating controller", CONTROLLER_NAME);

    $scope.items = [];
    $scope.SERVER_URL = CLIENT_SETTINGS.SERVER_URL;


    $scope.refreshItems = function() {
      itemSearchService.getNearbyItems().then(function(data) {
        $scope.items = data;
      });
    }

    $scope.$on('$ionicView.beforeEnter', function(){
      $scope.refreshItems();
    });

    $scope.goActionsDetail = function(itemId) {
      $state.go('tab.my-actions-detail', {itemId: itemId});
    }



  })
})();

