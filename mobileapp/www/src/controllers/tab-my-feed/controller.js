document.APP_MODULES = document.APP_MODULES || [];

(function(){

var CONTROLLER_URL = document.currentScript.src;
var TEMPLATE_URL = CONTROLLER_URL.replace('controller.js','view.html');
var CONTROLLER_PATH = URI(CONTROLLER_URL).path();
CONTROLLER_PATH = CONTROLLER_PATH.substring(CONTROLLER_PATH.indexOf('/src/controllers/'));

var ROUTE_URL = '/my-feed';
var MODULE_NAME = 'mainApp'+CONTROLLER_PATH.replace('/src','').replace('/controller.js','').replace(/\//g,'.');
var CONTROLLER_NAME = MODULE_NAME.replace(/\./g,'_').replace(/-/g,'_');
document.APP_MODULES.push(MODULE_NAME);

console.log(MODULE_NAME, "Registering route", ROUTE_URL);
angular.module(MODULE_NAME, ['ionic', 'ngStorage'])
  .config(function($stateProvider) {
    $stateProvider.state('tab.my-feed', {
        url: ROUTE_URL,
        views: {
          'tab-my-feed': {
            templateUrl: TEMPLATE_URL,
            controller: CONTROLLER_NAME
          }
        }
      });
  })
  .controller(CONTROLLER_NAME, function($scope, itemSearchService, $state, $timeout, $localStorage, userService, CLIENT_SETTINGS) {
    console.log("Instantiating controller", CONTROLLER_NAME);

    $scope.SERVER_URL = CLIENT_SETTINGS.SERVER_URL;
    $scope.feed_items = [];

    $scope.map = {};

    $scope.fetchFeedItems = function() {
      itemSearchService.getMyFeed().then(function(data) {
        $scope.feed_items = data;
      });
    }


    $scope.$on('$ionicView.beforeEnter', function(){
      $scope.fetchFeedItems();
      userService.getCurrentLocation().then(function(location) {
        $scope.map.zoom = 14;
        $scope.map.center = {latitude: location.coords.latitude, longitude: location.coords.longitude};
        console.log(location);
      });
    });

    $scope.$on('$ionicView.beforeLeave', function(){
    });

  })

})();

