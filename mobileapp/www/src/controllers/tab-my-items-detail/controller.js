document.APP_MODULES = document.APP_MODULES || [];

(function(){

var CONTROLLER_URL = document.currentScript.src;
var TEMPLATE_URL = CONTROLLER_URL.replace('controller.js','view.html');
var CONTROLLER_PATH = URI(CONTROLLER_URL).path();
CONTROLLER_PATH = CONTROLLER_PATH.substring(CONTROLLER_PATH.indexOf('/src/controllers/'));

var ROUTE_URL = '/my-items/:itemId';
var MODULE_NAME = 'mainApp'+CONTROLLER_PATH.replace('/src','').replace('/controller.js','').replace(/\//g,'.');
var CONTROLLER_NAME = MODULE_NAME.replace(/\./g,'_').replace(/-/g,'_');
document.APP_MODULES.push(MODULE_NAME);

console.log(MODULE_NAME, "Registering route", ROUTE_URL);
angular.module(MODULE_NAME, ['ionic'])
  .config(function($stateProvider) {
    $stateProvider.state('tab.my-items-detail', {
      url: ROUTE_URL,
      views: {
        'tab-my-items': {
          templateUrl: TEMPLATE_URL,
          controller: CONTROLLER_NAME
        }
      }
    });
  })
  .controller(CONTROLLER_NAME, function($scope, $stateParams, itemSearchService, userService, $state, CLIENT_SETTINGS) {
      $scope.SERVER_URL = CLIENT_SETTINGS.SERVER_URL;

      $scope.item = {};

      function distanceMiles(sourceCoord, destCoord) {
        var lat1 = sourceCoord.latitude;
        var lon1 = sourceCoord.longitude;
        var lat2 = destCoord.latitude;
        var lon2 = destCoord.longitude;
        var radlat1 = Math.PI * lat1/180
        var radlat2 = Math.PI * lat2/180
        var theta = lon1-lon2
        var radtheta = Math.PI * theta/180
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist)
        dist = dist * 180/Math.PI
        dist = dist * 60 * 1.1515
        return dist
      }


      $scope.$on('$ionicView.beforeEnter', function(){
        itemSearchService.getItem($stateParams.itemId).then(function(item) {
          userService.getCurrentLocation().then(function(pos) {
            var sourceCoords = pos.coords;
            var location = {'lat': pos.coords.latitude,
                             'lon': pos.coords.longitude,
                             'timestamp': (new Date()).toISOString()};
            var destCoords = item.location.coordinate;
            var travel = {};
            travel.distance_miles = distanceMiles(sourceCoords, destCoords)
            travel.travel_minutes = travel.distance_miles * 20;
            item.travel = travel;
            $scope.item = item;
            console.log(item);
          })
        });
      });

      $scope.$on('$ionicView.beforeLeave', function(){
        $scope.item = {};
      });
  })


})();

