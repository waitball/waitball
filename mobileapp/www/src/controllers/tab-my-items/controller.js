document.APP_MODULES = document.APP_MODULES || [];

(function(){

var CONTROLLER_URL = document.currentScript.src;
var TEMPLATE_URL = CONTROLLER_URL.replace('controller.js','view.html');
var CONTROLLER_PATH = URI(CONTROLLER_URL).path();
CONTROLLER_PATH = CONTROLLER_PATH.substring(CONTROLLER_PATH.indexOf('/src/controllers/'));

var ROUTE_URL = '/my-items/:maxTime/:itemId';
var MODULE_NAME = 'mainApp'+CONTROLLER_PATH.replace('/src','').replace('/controller.js','').replace(/\//g,'.');
var CONTROLLER_NAME = MODULE_NAME.replace(/\./g,'_').replace(/-/g,'_');
document.APP_MODULES.push(MODULE_NAME);

console.log(MODULE_NAME, "Registering route", ROUTE_URL);
angular.module(MODULE_NAME, ['ionic'])
  .config(function($stateProvider) {
    $stateProvider.state('tab.my-items', {
        url: ROUTE_URL,
        views: {
          'tab-my-items': {
            templateUrl: TEMPLATE_URL,
            controller: CONTROLLER_NAME
          }
        }
      });
  })
  .controller(CONTROLLER_NAME, function($scope, itemSearchService, $state, CLIENT_SETTINGS, $stateParams, $q, userService) {
    console.log("Instantiating controller", CONTROLLER_NAME);

    $scope.maxTimeMinutes = null;
    $scope.referenceItemId = null;
    $scope.referenceItem = null;


    $scope.searchResults = [];
    $scope.SERVER_URL = CLIENT_SETTINGS.SERVER_URL;
    $scope.loading = true;

    function distanceMiles(sourceCoord, destCoord) {
      var lat1 = sourceCoord.latitude;
      var lon1 = sourceCoord.longitude;
      var lat2 = destCoord.latitude;
      var lon2 = destCoord.longitude;
      var radlat1 = Math.PI * lat1/180
      var radlat2 = Math.PI * lat2/180
      var theta = lon1-lon2
      var radtheta = Math.PI * theta/180
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      dist = Math.acos(dist)
      dist = dist * 180/Math.PI
      dist = dist * 60 * 1.1515
      return dist
    }

    var prepareResults = function(searchResults) {
      console.log("Calculating distances");
      var deferred = $q.defer();
      userService.getCurrentLocation().then(function(pos) {
        var sourceCoords = pos.coords;
        var location = {'lat': pos.coords.latitude,
                         'lon': pos.coords.longitude,
                         'timestamp': (new Date()).toISOString()};
        _.each(searchResults, function(block) {
          _.each(block.businesses, function(business) {
            var destCoords = business.location.coordinate;
            var travel = {};
            travel.distance_miles = distanceMiles(sourceCoords, destCoords)
            travel.travel_minutes = travel.distance_miles * 20;
            business.travel = travel;
            console.log(business);
          })
        })
        deferred.resolve(searchResults);
      });
      return deferred.promise;
    }

    $scope.refreshItems = function() {
      if($scope.referenceItemId) {
        itemSearchService.getItem($scope.referenceItemId).then(function(item) {
          $scope.referenceItem = item;
        });
        $scope.loading = true;
        itemSearchService.getRecommendationsForItem($scope.referenceItemId).then(function(searchResults) {
          prepareResults(searchResults).then(function(searchResults) {
            $scope.searchResults = searchResults;
            $scope.loading = false;
          });
        });
      }
      else {
        $scope.loading = true;
        itemSearchService.getDefaultRecommendations().then(function(searchResults) {
          prepareResults(searchResults).then(function(searchResults) {
            $scope.searchResults = searchResults;
            $scope.loading = false;
          });
        });
      }


    }

    $scope.$on('$ionicView.beforeEnter', function(){
      $scope.maxTimeMinutes = Number($stateParams.maxTime) || null;
      $scope.referenceItemId = $stateParams.itemId || null;
      console.log($scope.maxTimeMinutes, $scope.referenceItemId);
      $scope.refreshItems();
    });

    $scope.$on('$ionicView.beforeLeave', function(){
      $scope.searchResults = [];
    });

    $scope.goItemDetail = function(itemId) {
      $state.go('tab.my-items-detail', {itemId: itemId});
    }

  })

  
})();

